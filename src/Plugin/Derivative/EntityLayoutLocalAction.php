<?php

namespace Drupal\entity_layout\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\entity_layout\EntityLayoutManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EntityLayoutLocalAction extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The route provider.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * The entity manager
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity layout manager.
   *
   * @var EntityLayoutManager
   */
  private $entityLayoutManager;

  /**
   * Creates an EntityLayoutLocalTask object.
   *
   * @param RouteProviderInterface $routeProvider
   *   The route provider.
   *
   * @param EntityTypeManagerInterface $entityTypeManager
   *   The entity manager.
   *
   * @param TranslationInterface $stringTranslation
   *   The translation manager.
   *
   * @param EntityLayoutManager $entityLayoutManager
   *   The entity layout manager.
   */
  public function __construct(
    RouteProviderInterface $routeProvider,
    EntityTypeManagerInterface $entityTypeManager,
    TranslationInterface $stringTranslation,
    EntityLayoutManager $entityLayoutManager
  ) {
    $this->routeProvider = $routeProvider;
    $this->entityTypeManager = $entityTypeManager;
    $this->stringTranslation = $stringTranslation;
    $this->entityLayoutManager = $entityLayoutManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('router.route_provider'),
      $container->get('entity.manager'),
      $container->get('string_translation'),
      $container->get('entity_layout.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = array();

    foreach ($this->entityLayoutManager->getAll() as $entity_layout) {
      $entity_type_id = $entity_layout
        ->getTargetEntityType();

      $entity_type = $this->entityTypeManager
        ->getDefinition($entity_type_id);

      if (!$route_name = $entity_type->get('field_ui_base_route')) {
        continue;
      }

      $this->derivatives["entity_layout_{$entity_type_id}_block_add"] = [
        'route_name' => "entity_layout.$entity_type_id.layout",
        'title' => $this->t('Add block'),
        'appears_on' => "entity_layout.$entity_type_id.layout",
      ] + $base_plugin_definition;
    }

    return $this->derivatives;
  }
}
